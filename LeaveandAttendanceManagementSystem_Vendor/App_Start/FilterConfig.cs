﻿using System.Web;
using System.Web.Mvc;

namespace LeaveandAttendanceManagementSystem_Vendor
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
