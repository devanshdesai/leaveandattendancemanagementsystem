﻿using System.Collections.Generic;

namespace LeaveandAttendanceManagementSystem.Common.Paging
{
    /// <summary>
    /// Paged list interface
    /// </summary>
    public interface IPagedList<T> : IList<T>
    {
        
    }
}
